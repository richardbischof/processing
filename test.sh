#!/usr/bin/env bash

#java -jar ./target/rectify-1.0-SNAPSHOT-jar-with-dependencies.jar \
#    -input_file ./data/image.tif \
#    -crop_file ./data/out/image_crop.tif \
#    -rectified_file ./data/out/image_rectified.tif \
#    -crop_data ./data/crop.json \
#    -gcps_data ./data/gcps.json \
#    -footprint_file ./data/out/footprint.geojson \
#    -error_file ./data/out/error.json \
#    -resampling_type BILINEAR \
#    -transformation_method SECOND_ORDER \
#    -buffer 200

docker run --rm -v $(pwd)/data:/data processing \
    -input_file /data/image.tif \
    -crop_file /data/out/image_crop.tif \
    -rectified_file /data/out/image_rectified.tif \
    -crop_data /data/crop.json \
    -gcps_data /data/gcps.json \
    -footprint_file /data/out/footprint.geojson \
    -error_file /data/out/error.json \
    -resampling_type BILINEAR \
    -transformation_method SECOND_ORDER
