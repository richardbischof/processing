FROM maven:3-jdk-11-slim as builder
WORKDIR /src
COPY pom.xml /src
RUN mvn install
COPY . /src
RUN mvn package

FROM openjdk:11-jdk-slim
COPY --from=builder /src/target/rectify-1.0-SNAPSHOT-jar-with-dependencies.jar /app/processing.jar
ENTRYPOINT ["java","-jar","/app/processing.jar"]
