package rectification;

/**
 * @author richardbischof
 */
public class GroundControlPoint {
    public double srcx, srcy, dstx, dsty;

    public GroundControlPoint(double srcx, double srcy, double dstx, double dsty) {
        this.srcx = srcx;
        this.srcy = srcy;
        this.dstx = dstx;
        this.dsty = dsty;
    }

    public double getSrcx() {
        return srcx;
    }

    public void setSrcx(double srcx) {
        this.srcx = srcx;
    }

    public double getSrcy() {
        return srcy;
    }

    public void setSrcy(double srcy) {
        this.srcy = srcy;
    }

    public double getDstx() {
        return dstx;
    }

    public void setDstx(double dstx) {
        this.dstx = dstx;
    }

    public double getDsty() {
        return dsty;
    }

    public void setDsty(double dsty) {
        this.dsty = dsty;
    }

    @Override
    public String toString() {
        return "rectification.GroundControlPoint{" +
                "srcx=" + srcx +
                ", srcy=" + srcy +
                ", dstx=" + dstx +
                ", dsty=" + dsty +
                '}';
    }
}
