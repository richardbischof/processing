package rectification.transformation;

import rectification.GroundControlPoint;

import java.util.List;

/**
 * @author richardbischof
 */
public class TransformationFactory {

    public static Transformation create(TransformationMethod transformationMethod, List<GroundControlPoint> groundControlPointList) {

        Transformation transformation;

        switch (transformationMethod) {

            case POLYNOMIAL_FIRST_ORDER:
                transformation = new TransformationFirstOrder(groundControlPointList);
                break;
            case POLYNOMIAL_SECOND_ORDER:
                transformation = new TransformationSecondOrder(groundControlPointList);
                break;
            default:
                throw new IllegalArgumentException("TransformationMethod is not valid.");
        }

        return transformation;
    }
}
