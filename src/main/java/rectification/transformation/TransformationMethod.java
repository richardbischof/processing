package rectification.transformation;

/**
 * @author richardbischof
 */
public enum TransformationMethod {
    FIRST_ORDER,
    SECOND_ORDER
}
