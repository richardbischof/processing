package rectification.transformation;

import com.vividsolutions.jts.geom.Coordinate;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import rectification.GroundControlPoint;

import java.util.List;


public class TransformationSecondOrder extends TransformationBase {

    public TransformationSecondOrder(List<GroundControlPoint> gcps) {
        super(gcps);
        this.transform();
    }

    @Override
    public Coordinate transformCoordinate(Coordinate c) {

        Coefficient k = this.coefficient;

        double xT = k.dxxx * Math.pow(c.x, 2) + k.dxyy * Math.pow(c.y, 2) + k.dxxy * c.x * c.y + k.dxx * c.x + k.dxy * c.y + k.dx;
        double yT = k.dyxx * Math.pow(c.x, 2) + k.dyyy * Math.pow(c.y, 2) + k.dyxy * c.x * c.y + k.dyx * c.x + k.dyy * c.y + k.dy;

        return new Coordinate(xT, yT);
    }

    /**
     * D = srcX srcY 1
     * R = dstX
     * C = dstY
     * <p>
     * KoeffX = (D^t * D )^-1 * D^t * R
     * KoeffY = (D^t * D )^-1 * D^t * C
     */
    public void transform() {

        int k = this.groundControlPointList.size();

        double[][] D = new double[k][6];
        double[][] r = new double[k][1];
        double[][] c = new double[k][1];

        for (int i = 0; i < k; i++) {
            GroundControlPoint gcp = this.groundControlPointList.get(i);

            D[i][0] = Math.pow(gcp.srcx, 2);
            D[i][1] = Math.pow(gcp.srcy, 2);
            D[i][2] = gcp.srcx * gcp.srcy;
            D[i][3] = gcp.srcx;
            D[i][4] = gcp.srcy;
            D[i][5] = 1;
            r[i][0] = gcp.dstx;
            c[i][0] = gcp.dsty;
        }

        RealMatrix d = MatrixUtils.createRealMatrix(D);
        RealMatrix R = MatrixUtils.createRealMatrix(r);
        RealMatrix C = MatrixUtils.createRealMatrix(c);

        RealMatrix dTrans = d.transpose();
        RealMatrix dTrans_d = dTrans.multiply(d);
        RealMatrix dTrans_d_Inverse = new LUDecomposition(dTrans_d).getSolver().getInverse();
        RealMatrix dTrans_d_Inverse_dTrans = dTrans_d_Inverse.multiply(dTrans);

        RealMatrix A = dTrans_d_Inverse_dTrans.multiply(R);
        RealMatrix B = dTrans_d_Inverse_dTrans.multiply(C);

        double[] koefx = A.getColumn(0);
        double[] koefy = B.getColumn(0);

        this.coefficient = new Coefficient();

        this.coefficient.dxxx = koefx[0];
        this.coefficient.dxyy = koefx[1];
        this.coefficient.dxxy = koefx[2];
        this.coefficient.dxx = koefx[3];
        this.coefficient.dxy = koefx[4];
        this.coefficient.dx = koefx[5];

        this.coefficient.dyxx = koefy[0];
        this.coefficient.dyyy = koefy[1];
        this.coefficient.dyxy = koefy[2];
        this.coefficient.dyx = koefy[3];
        this.coefficient.dyy = koefy[4];
        this.coefficient.dy = koefy[5];


    }

}
