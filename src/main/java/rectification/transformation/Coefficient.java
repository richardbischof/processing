package rectification.transformation;

/**
 * Class Coefficient
 * for storing Coefficient used by transformation algorithms
 *
 * @author Richard.Bischof
 */
public class Coefficient {

    public double dxxx = 0;
    public double dxyy = 0;
    public double dxxy = 0;
    public double dxx = 0;
    public double dxy = 0;
    public double dx = 0;

    public double dyxx = 0;
    public double dyyy = 0;
    public double dyxy = 0;
    public double dyx = 0;
    public double dyy = 0;
    public double dy = 0;

    @Override
    public String toString() {
        return "Coefficient [dxxx=" + dxxx + ", dxyy=" + dxyy + ", dxxy=" + dxxy + ", dxx=" + dxx + ", dxy=" + dxy
                + ", dx=" + dx + ", dyxx=" + dyxx + ", dyyy=" + dyyy + ", dyxy=" + dyxy + ", dyx=" + dyx + ", dyy="
                + dyy + ", dy=" + dy + "]";
    }

}
