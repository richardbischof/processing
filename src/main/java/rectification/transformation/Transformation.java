package rectification.transformation;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import rectification.WorldData;

/**
 * @author richardbischof
 */
public interface Transformation {

    Coordinate transformCoordinate(Coordinate coordinate);

    double getRootMeanSquareError();

    Coefficient getCoefficients();

    Polygon getFootprint(int widthSource, int heightSource);

    WorldData getTargetWorldDataWithBuffer(int widthSource, int heightSource, int buffer);

}
