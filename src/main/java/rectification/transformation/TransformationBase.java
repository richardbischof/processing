package rectification.transformation;

import com.vividsolutions.jts.geom.*;
import rectification.GroundControlPoint;
import rectification.WorldData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author richardbischof
 */
public abstract class TransformationBase implements Transformation {


    protected List<GroundControlPoint> groundControlPointList;
    protected Coefficient coefficient;

    public TransformationBase(List<GroundControlPoint> groundControlPointList) {
        this.groundControlPointList = groundControlPointList;
    }

    @Override
    public Coordinate transformCoordinate(Coordinate coordinate) {
        return null;
    }

    @Override
    public double getRootMeanSquareError() {
        double sum = 0;
        for (GroundControlPoint gcp : groundControlPointList) {
            Coordinate src = new Coordinate(gcp.srcx, gcp.srcy);
            Coordinate srcT = transformCoordinate(src);
            Coordinate dst = new Coordinate(gcp.dstx, gcp.dsty);
            double dist = dst.distance(srcT);
            sum += Math.pow(dist, 2.0);
        }
        return Math.sqrt(sum / this.groundControlPointList.size());
    }

    @Override
    public Coefficient getCoefficients() {
        return this.coefficient;
    }


    @Override
    public WorldData getTargetWorldDataWithBuffer(int widthSource, int heightSource, int buffer) {
        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double maxY = Double.MIN_VALUE;

        Coordinate c1 = new Coordinate(0, 0);
        Coordinate c2 = new Coordinate(widthSource, 0);
        Coordinate c3 = new Coordinate(widthSource, heightSource);
        Coordinate c4 = new Coordinate(0, heightSource);

        List<Coordinate> pointList = new ArrayList<Coordinate>(Arrays.asList(c1, c2, c3, c4));

        ArrayList<Coordinate> coordinates = new ArrayList<Coordinate>();

        for (Coordinate c : pointList) {

            Coordinate p = this.transformCoordinate(c);
            coordinates.add(p);

            if (p.x + buffer > maxX) {
                maxX = p.x + buffer;
            }
            if (p.x - buffer < minX) {
                minX = p.x - buffer;
            }
            if (p.y + buffer > maxY) {
                maxY = p.y + buffer;
            }
            if (p.y - buffer < minY) {
                minY = p.y - buffer;
            }
        }

        GeometryFactory factory = new GeometryFactory();
        Coordinate[] coord = new Coordinate[5];
        for (int i = 0; i < coord.length - 1; i++) {
            coord[i] = coordinates.get(i);
        }
        coord[4] = coordinates.get(0);
        Polygon polygon = factory.createPolygon(coord);

        double pixelWidthInMeter = this.getAverageHorizontalPixelSize(widthSource, heightSource);
        double pixelHeightInMeter = this.getAverageVerticalPixelSize(widthSource, heightSource);

        WorldData targetWorldData = new WorldData(minX, maxX, minY, maxY, pixelWidthInMeter, pixelHeightInMeter);
        targetWorldData.setFootprint(polygon);
        return targetWorldData;
    }

    @Override
    public Polygon getFootprint(int widthSource, int heightSource) {
        Coordinate c1 = this.transformCoordinate(new Coordinate(0, 0));
        Coordinate c2 = this.transformCoordinate(new Coordinate(0, widthSource));
        Coordinate c3 = this.transformCoordinate(new Coordinate(heightSource, widthSource));
        Coordinate c4 = this.transformCoordinate(new Coordinate(heightSource, 0));

        Coordinate[] coordinates = new Coordinate[]{c1, c2, c3, c4, c1};
        GeometryFactory factory = new GeometryFactory();
        LinearRing ring = factory.createLinearRing(coordinates);
        Polygon footprint = factory.createPolygon(ring);
        footprint.setSRID(25832);
        return footprint;
    }

    public double getAverageHorizontalPixelSize(int widthSource, int heightSource) {
        int counter = 0;
        double distanceTotal = 0;

        for (int x = 1; x < heightSource; x += 1000) {
            for (int y = 0; y < widthSource; y += 1000) {
                Coordinate c1 = this.transformCoordinate(new Coordinate(x, y * -1));
                Coordinate c2 = this.transformCoordinate(new Coordinate(x + 1, y * -1));

                if (c1 != null && c2 != null) {
                    counter++;
                    distanceTotal += c1.distance(c2);
                }
            }
        }
        return Math.round(distanceTotal / counter * 100.0) / 100.0;

    }


    public double getAverageVerticalPixelSize(int widthSource, int heightSource) {

        int counter = 0;
        double distanceTotal = 0;

        for (int x = 1; x < heightSource; x += 1000) {
            for (int y = 0; y < widthSource; y += 1000) {
                Coordinate c1 = this.transformCoordinate(new Coordinate(x, y * -1));
                Coordinate c2 = this.transformCoordinate(new Coordinate(x, (y + 1) * -1));

                if (c1 != null && c2 != null) {
                    counter++;
                    distanceTotal += c1.distance(c2);
                }

            }

        }
        return Math.round(distanceTotal / counter * 100.0) / 100.0;
    }

    protected List<GroundControlPoint> getInverseGCPList(){
        return groundControlPointList.stream().map( gcp -> invertGroundControlPoint(gcp)).collect(Collectors.toList());
    }

    protected GroundControlPoint invertGroundControlPoint(GroundControlPoint gcp){
        return new GroundControlPoint(gcp.dstx, gcp.dsty, gcp.srcx, gcp.srcy);
    };


}
