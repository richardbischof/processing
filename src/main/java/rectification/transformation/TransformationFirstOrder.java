package rectification.transformation;

import com.vividsolutions.jts.geom.Coordinate;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import rectification.GroundControlPoint;

import java.util.List;

/**
 * @author richardbischof
 */
public class TransformationFirstOrder extends TransformationBase {

    public TransformationFirstOrder(List<GroundControlPoint> gcps) {
        super(gcps);
        this.transform();
    }

    @Override
    public Coordinate transformCoordinate(Coordinate c) {

        Coefficient k = this.coefficient;

        double xT = k.dxx * c.x + k.dxy * c.y + k.dx;
        double yT = k.dyx * c.x + k.dyy * c.y + k.dy;


        return new Coordinate(xT, yT);
    }

    /**
     * D = srcX srcY 1
     * R = dstX
     * C = dstY
     * <p>
     * KoeffX = (D^t * D )^-1 * D^t * R
     * KoeffY = (D^t * D )^-1 * D^t * C
     */
    public void transform() {

        this.groundControlPointList = getInverseGCPList();

        int k = this.groundControlPointList.size();

        double[][] D = new double[k][3];
        double[][] r = new double[k][1];
        double[][] c = new double[k][1];

        for (int i = 0; i < k; i++) {
            GroundControlPoint gcp = this.groundControlPointList.get(i);

            D[i][0] = gcp.srcx;
            D[i][1] = gcp.srcy;
            D[i][2] = 1.0;
            r[i][0] = gcp.dstx;
            c[i][0] = gcp.dsty;
        }

        RealMatrix d = MatrixUtils.createRealMatrix(D);
        RealMatrix R = MatrixUtils.createRealMatrix(r);
        RealMatrix C = MatrixUtils.createRealMatrix(c);

        RealMatrix dTrans = d.transpose();
        RealMatrix dTrans_d = dTrans.multiply(d);
        RealMatrix dTrans_d_Inverse = new LUDecomposition(dTrans_d).getSolver().getInverse();
        RealMatrix dTrans_d_Inverse_dTrans = dTrans_d_Inverse.multiply(dTrans);

        RealMatrix A = dTrans_d_Inverse_dTrans.multiply(R);
        RealMatrix B = dTrans_d_Inverse_dTrans.multiply(C);

        double[] a = A.getColumn(0);
        double[] b = B.getColumn(0);

        double[][] koef = new double[3][3];
        koef[0][0] = a[0];
        koef[0][1] = a[1];
        koef[0][2] = a[2];
        koef[1][0] = b[0];
        koef[1][1] = b[1];
        koef[1][2] = b[2];
        koef[2][0] = 0;
        koef[2][1] = 0;
        koef[2][2] = 1;

        RealMatrix koeft = MatrixUtils.createRealMatrix(koef);
        RealMatrix koefInv = new LUDecomposition(koeft).getSolver().getInverse();

        double[] koefx = koefInv.getRow(0);
        double[] koefy = koefInv.getRow(1);

        this.coefficient = new Coefficient();
        this.coefficient.dxx = koefx[0];
        this.coefficient.dxy = koefx[1];
        this.coefficient.dx = koefx[2];

        this.coefficient.dyx = koefy[0];
        this.coefficient.dyy = koefy[1];
        this.coefficient.dy = koefy[2];

    }
}
