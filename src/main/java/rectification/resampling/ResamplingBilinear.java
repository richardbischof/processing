package rectification.resampling;

import java.awt.image.BufferedImage;

/**
 * @author richardbischof
 */
public class ResamplingBilinear extends ResamplingImpl {
    @Override
    public double getPixelValue(BufferedImage image, double x, double y, double xxMin, double yyMin, double dx, double dy) {
        // (x,y) coordinates of value to be interpolated
        // indices of closest sample points in x direction, x0 <= x <= x1
        int i0 = (int) ((x - xxMin) / dx);
        i0 = Math.min(Math.max(i0, 0), image.getWidth() - 1);    // ensure 0 <= i0 < xSize
        int i1 = Math.min(i0 + 1, image.getWidth() - 1);
        // indices of closest sample points in y direction, y0 <= y <= y1
        int j0 = (int) ((y - yyMin) / dy);
        j0 = Math.min(Math.max(j0, 0), image.getHeight() - 1);    // ensure 0 <= j0 < ySize
        int j1 = Math.min(j0 + 1, image.getHeight() - 1);
        double r = (x - (xxMin + i0 * dx)) / dx;
        double s = (y - (yyMin + j0 * dy)) / dy;
        int c1 = image.getRGB(i0, j0) & 0xFF;
        int c2 = image.getRGB(i1, j0) & 0xFF;
        int c3 = image.getRGB(i1, j1) & 0xFF;
        int c4 = image.getRGB(i0, j1) & 0xFF;
        double color = (1 - r) * (1 - s) * c1 + r * (1 - s) * c2 + r * s * c3 + (1 - r) * s * c4;
        return color;
    }
}
