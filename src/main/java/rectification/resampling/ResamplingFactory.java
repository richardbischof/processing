package rectification.resampling;

/**
 * @author richardbischof
 */
public class ResamplingFactory {

    public static Resampling create(ResamplingType type){
        Resampling resampling;
        switch (type) {
            case NEAREST_NEIGHBOUR:
                resampling = new ResamplingNearestNeighbour();
                break;
            case BILINEAR:
                resampling = new ResamplingBilinear();
                break;
            default:
                throw new IllegalArgumentException("Can not create Resampling.");
        }
        return resampling;
    }
}
