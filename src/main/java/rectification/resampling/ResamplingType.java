package rectification.resampling;

/**
 * @author richardbischof
 */
public enum ResamplingType {
    NEAREST_NEIGHBOUR,
    BILINEAR
}
