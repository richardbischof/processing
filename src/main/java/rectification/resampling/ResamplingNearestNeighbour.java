package rectification.resampling;

import java.awt.image.BufferedImage;

/**
 * @author richardbischof
 */
public class ResamplingNearestNeighbour extends ResamplingImpl {

    @Override
    public double getPixelValue(BufferedImage image, double x, double y, double xMin, double yMin, double dx, double dy) {
        return image.getRGB((int) x,(int)y);
    }
}
