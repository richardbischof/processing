package rectification.resampling;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author richardbischof
 */
public interface Resampling {

    double getPixelValue(BufferedImage image, double x, double y, double xMin, double yMin, double dx, double dy);
}
