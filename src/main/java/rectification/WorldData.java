package rectification;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import rectification.transformation.Coefficient;

/**
 * World Data represents the geoinformation metadata for images
 *
 * @author Richard.Bischof
 */
public class WorldData {

    private Point topleft;
    private Polygon footprint;
    private Double pixelWidthInMeter, pixelHeightInMeter;
    private Double minX, maxX, minY, maxY;
    private Double skewX = 0.0;
    private Double skewY = 0.0;

    public WorldData(Point topleft, Polygon footprint) {
        super();
        this.topleft = topleft;
        this.footprint = footprint;
    }

    public WorldData(
            double minX,
            double maxX,
            double minY,
            double maxY,
            double pixelWidthInMeter,
            double pixelHeightInMeter) {

        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.pixelWidthInMeter = pixelWidthInMeter;
        this.pixelHeightInMeter = pixelHeightInMeter;
    }

    public WorldData(Double pixelWidthInMeter, Double skewX,
                     Double skewY, Double pixelHeightInMeter, Double minX, Double maxY) {
        super();
        this.pixelWidthInMeter = pixelWidthInMeter;
        this.pixelHeightInMeter = pixelHeightInMeter;
        this.minX = minX;
        this.maxY = maxY;
        this.skewX = skewX;
        this.skewY = skewY;
    }

    public WorldData(Coefficient koef) {
        this.pixelWidthInMeter = koef.dxx;
        this.skewX = koef.dxy;
        this.skewY = koef.dyx;
        this.pixelHeightInMeter = -koef.dyy;
        this.minX = koef.dx;
        this.maxY = koef.dy;
    }

    public int getHeightInPixel() {
        return (int) ((this.maxY - this.minY) / this.pixelHeightInMeter);

    }

    public int getWidthInPixel() {
        return (int) ((this.maxX - this.minX) / this.pixelWidthInMeter);
    }

    public double getPixelWidthInMeter() {
        return pixelWidthInMeter;
    }

    public void setPixelWidthInMeter(double pixelWidthInMeter) {
        this.pixelWidthInMeter = pixelWidthInMeter;
    }

    public double getPixelHeightInMeter() {
        return pixelHeightInMeter;
    }

    public void setPixelHeightInMeter(double pixelHeightInMeter) {
        this.pixelHeightInMeter = pixelHeightInMeter;
    }

    public Point getTopleft() {
        return topleft;
    }

    public void setTopleft(Point topleft) {
        this.topleft = topleft;
    }

    public Polygon getFootprint() {
        return footprint;
    }

    public void setFootprint(Polygon footprint) {
        this.footprint = footprint;
    }

    public double getMinX() {
        if (this.minX == null) {
            double minX = Double.MAX_VALUE;
            for (Coordinate c : this.footprint.getCoordinates()) {
                if (c.x < minX) minX = c.x;
            }
            this.minX = minX;
        }
        return this.minX;
    }

    public void setMinX(double minX) {
        this.minX = minX;
    }

    public double getMaxX() {
        if (this.maxX == null) {
            double maxX = Double.MIN_VALUE;
            for (Coordinate c : this.footprint.getCoordinates()) {
                if (c.x > maxX) maxX = c.x;
            }
            this.maxX = maxX;
        }
        return this.maxX;
    }

    public void setMaxX(double maxX) {
        this.maxX = maxX;
    }

    public double getMinY() {
        if (this.minY == null) {
            double minY = Double.MAX_VALUE;
            for (Coordinate c : this.footprint.getCoordinates()) {
                if (c.x < minY) minY = c.x;
            }
            this.minY = minY;
        }
        return this.minY;
    }

    public void setMinY(double minY) {
        this.minY = minY;
    }

    public double getMaxY() {
        if (this.maxY == null) {
            double maxY = Double.MIN_VALUE;
            for (Coordinate c : this.footprint.getCoordinates()) {
                if (c.x > maxY) maxY = c.x;
            }
            this.maxY = maxY;
        }
        return this.maxY;
    }

    public void setMaxY(double maxY) {
        this.maxY = maxY;
    }

    public String getWorldFile() {
        String worldFile = "";
        worldFile += this.pixelWidthInMeter + "\n";
        worldFile += this.skewY + "\n";
        worldFile += this.skewX + "\n";
        worldFile += -this.pixelHeightInMeter + "\n";
        worldFile += this.minX + "\n";
        worldFile += this.maxY + "\n";
        return worldFile;
    }


    @Override
    public String toString() {
        return "WorldData [topleft=" + topleft + ", footprint=" + footprint + ", pixelWidthInMeter=" + pixelWidthInMeter
                + ", pixelHeightInMeter=" + pixelHeightInMeter + ", minX=" + minX + ", maxX=" + maxX + ", minY=" + minY
                + ", maxY=" + maxY + ", skewX=" + skewX + ", skewY=" + skewY + "]";
    }


}
