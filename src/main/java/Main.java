import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import it.geosolutions.imageio.plugins.tiff.BaselineTIFFTagSet;
import it.geosolutions.imageio.plugins.tiff.FaxTIFFTagSet;
import it.geosolutions.imageio.plugins.tiff.TIFFImageReadParam;
import job.Image;
import job.Job;
import job.JobFactory;
import job.JobType;
import job.crop.CropData;
import job.crop.CropJobParameters;
import job.rectify.RectifyJobParameters;
import org.apache.commons.cli.*;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.imageio.GeoToolsWriteParams;
import org.geotools.gce.geotiff.GeoTiffFormat;
import org.geotools.gce.geotiff.GeoTiffWriteParams;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.CRS;
import org.opengis.coverage.grid.GridCoverageWriter;
import org.opengis.geometry.Envelope;
import org.opengis.parameter.GeneralParameterValue;
import org.opengis.parameter.ParameterValueGroup;
import org.opengis.referencing.FactoryException;
import org.wololo.geojson.GeoJSON;
import org.wololo.jts2geojson.GeoJSONWriter;
import rectification.GroundControlPoint;
import rectification.resampling.ResamplingType;
import rectification.transformation.TransformationMethod;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

/**
 * @author richardbischof
 */
public class Main {

    private static Image original;
    private static Image cropped;
    private static Image rectified;

    private static URI pathname_original, pathname_crop, pathname_rectified;
    private static String pathname_cropdata, pathname_gcpdata;
    private static String pathname_footprint, pathname_error;

    private static ResamplingType resamplingType;
    private static TransformationMethod transformationMethod;
    private static int buffer;

    private static final String EPSG25832 = "PROJCS[\"ETRS89 / UTM zone 32N\",GEOGCS[\"ETRS89\",DATUM[\"European_Terrestrial_Reference_System_1989\",SPHEROID[\"GRS 1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY[\"EPSG\",\"6258\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4258\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",9],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"25832\"]]";


    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        System.setProperty("hsqldb.reconfig_logging", "false");

        Options options = new Options();

        options.addOption("input_file",true, "input file");
        options.addOption("crop_file",true, "crop file");
        options.addOption("rectified_file",true, "rectified file");
        options.addOption("crop_data",true, "crop parameter file");
        options.addOption("gcps_data",true, "rectification parameter file");
        options.addOption("footprint_file", true, "rectification footprint file");
        options.addOption("error_file",true, "rectification error file");
        options.addOption("resampling_type", true, "resampling method");
        options.addOption("transformation_method", true, "transformation method");
        options.addOption("buffer", true, "buffer");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("input_file")){
                try {
                    pathname_original = new URI(line.getOptionValue("input_file"));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }

            if (line.hasOption("crop_file")){
                try {
                    pathname_crop = new URI(line.getOptionValue("crop_file"));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }

            if (line.hasOption("rectified_file")){
                try {
                    pathname_rectified = new URI(line.getOptionValue("rectified_file"));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }

            if (line.hasOption("crop_data")){
                pathname_cropdata = line.getOptionValue("crop_data");
            }

            if (line.hasOption("gcps_data")){
                pathname_gcpdata = line.getOptionValue("gcps_data");
            }

            if (line.hasOption("footprint_file")){
                pathname_footprint = line.getOptionValue("footprint_file");
            }

            if (line.hasOption("error_file")){
                pathname_error = line.getOptionValue("error_file");
            }

            if (line.hasOption("resampling_type")){
                resamplingType = ResamplingType.valueOf(line.getOptionValue("resampling_type"));
            }

            if (line.hasOption("transformation_method")){
                transformationMethod = TransformationMethod.valueOf(line.getOptionValue("transformation_method"));
            }

            if (line.hasOption("buffer")){
                buffer = Integer.valueOf(line.getOptionValue("buffer"));
            }

        } catch (ParseException e) {
            System.err.println("Parsing failed. Reason: " + e.getMessage());
        }

        original = load(pathname_original);

        JobFactory fac = new JobFactory();

        CropData cropData = parseCropDataJson(pathname_cropdata);
        Job crop = fac.create(JobType.CROP, original, new CropJobParameters(cropData));
        cropped = crop.run();

        write(cropped, pathname_crop);

        List<GroundControlPoint> groundControlPoint = parseGroundControlPointsJson(pathname_gcpdata);
        job.Job rectify = fac.create(
                job.JobType.RECTIFY,
                cropped, new RectifyJobParameters(
                        groundControlPoint,
                        transformationMethod,
                        resamplingType,
                        buffer));
        rectified = rectify.run();

        GeoJSONWriter writer = new GeoJSONWriter();
        GeoJSON json = writer.write(rectified.getFootprint());
        String jsonstring = json.toString();

        try (PrintWriter out = new PrintWriter(pathname_footprint)) {
            out.write(jsonstring);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } ;

        JsonObject errorObject = new JsonObject();
        errorObject.addProperty("rms",rectified.getRootMeanSquareError());

        try (PrintWriter out = new PrintWriter(pathname_error)) {
            out.write(errorObject.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        write(Main.rectified, pathname_rectified);

        long duration = System.currentTimeMillis() - start;

        System.out.println("Finished in " + duration +" ms." );
    }

    private static Image load(URI uri) {
        try {
            File file = new File(uri.toString());
            InputStream stream = new FileInputStream(file);

            ImageReader tiffReader = ImageIO.getImageReadersByFormatName("tiff").next();

            ImageInputStream input = ImageIO.createImageInputStream(stream);
            tiffReader.setInput(input);
            TIFFImageReadParam mTIFFImageReadParam = new TIFFImageReadParam();

            mTIFFImageReadParam.removeAllowedTagSet(BaselineTIFFTagSet.getInstance());
            mTIFFImageReadParam.removeAllowedTagSet(FaxTIFFTagSet.getInstance());

            long s = System.currentTimeMillis();
            BufferedImage image = tiffReader.read(0, mTIFFImageReadParam);
            long d = System.currentTimeMillis() - s;
            System.out.println("Read of " + uri + " took " + d/1000 + " s");

            IIOMetadata metaData = tiffReader.getImageMetadata(0);

            return new Image(metaData, image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Could not load input image" + uri);
    }

    private static void write(job.Image image, URI uri) {
        try {
            GeoTiffFormat format = new GeoTiffFormat();
            GridCoverageWriter writer = format.getWriter(new File(uri.toString()));

            GeoTiffWriteParams geoTiffWriteParams = new GeoTiffWriteParams();

            geoTiffWriteParams.setCompressionMode(GeoTiffWriteParams.MODE_EXPLICIT);
            geoTiffWriteParams.setCompressionType("LZW");
            geoTiffWriteParams.setCompressionQuality(1);

            geoTiffWriteParams.setTilingMode(GeoToolsWriteParams.MODE_EXPLICIT);
            geoTiffWriteParams.setTiling(256, 256);

            ParameterValueGroup params = format.getWriteParameters();
            params.parameter(AbstractGridFormat.GEOTOOLS_WRITE_PARAMS.getName().toString()).setValue(geoTiffWriteParams);

            GridCoverageFactory factory = new GridCoverageFactory();

            Envelope envelope = image.getEnvelope() != null ? image.getEnvelope() : new ReferencedEnvelope(0,1000,0,1000,CRS.parseWKT(EPSG25832));
            GridCoverage2D gridCoverage2D = factory.create("GridCoverage",image.getImage(), envelope);
            long s = System.currentTimeMillis();
            writer.write(gridCoverage2D, (GeneralParameterValue[]) params.values().toArray(new GeneralParameterValue[1]));
            long duration = System.currentTimeMillis() - s;
            System.out.println("File write of " + uri + " took " + duration/1000 + " s.");
        } catch (IOException | FactoryException e) {
            e.printStackTrace();
        }
    }

    private static CropData parseCropDataJson(String pathname){
        Gson g = new Gson();
        CropData cropData;
        try {
            JsonReader reader = new JsonReader(new FileReader(new File(pathname)));
            cropData = g.fromJson(reader, CropData.class);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Cant read crop data from file.");
        }
        return cropData;
    }

    private static List<GroundControlPoint> parseGroundControlPointsJson(String pathname){
        Gson g = new Gson();
        GroundControlPoint[] groundControlPointList;
        try {
            JsonReader reader = new JsonReader(new FileReader(pathname));
            groundControlPointList = g.fromJson(reader, GroundControlPoint[].class);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Cant read gcps from file.");
        }

        return Arrays.asList(groundControlPointList);
    }

}
