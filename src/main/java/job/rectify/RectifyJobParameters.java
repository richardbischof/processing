package job.rectify;

import job.JobParameter;
import rectification.GroundControlPoint;
import rectification.resampling.ResamplingType;
import rectification.transformation.TransformationMethod;

import java.util.List;

/**
 * @author richardbischof
 */
public class RectifyJobParameters extends JobParameter {

    private List<GroundControlPoint> groundControlPointList;
    private TransformationMethod transformationMethod;
    private ResamplingType resamplingType;
    private int buffer;

    public RectifyJobParameters(List<GroundControlPoint> groundControlPointList, TransformationMethod transformationMethod, ResamplingType resamplingType, int buffer) {
        this.groundControlPointList = groundControlPointList;
        this.transformationMethod = transformationMethod;
        this.resamplingType = resamplingType;
        this.buffer = buffer;
    }

    public List<GroundControlPoint> getGroundControlPointList() {
        return groundControlPointList;
    }

    public void setGroundControlPointList(List<GroundControlPoint> groundControlPointList) {
        this.groundControlPointList = groundControlPointList;
    }

    public TransformationMethod getTransformationMethod() {
        return transformationMethod;
    }

    public void setTransformationMethod(TransformationMethod transformationMethod) {
        this.transformationMethod = transformationMethod;
    }

    public ResamplingType getResamplingType() {
        return resamplingType;
    }

    public void setResamplingType(ResamplingType resamplingType) {
        this.resamplingType = resamplingType;
    }

    public int getBuffer() {
        return this.buffer;
    }
}
