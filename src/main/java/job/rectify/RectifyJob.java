package job.rectify;

import job.Image;
import job.JobBase;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.CRS;
import org.opengis.geometry.Envelope;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import rectification.*;
import rectification.transformation.Coefficient;
import rectification.resampling.Resampling;
import rectification.resampling.ResamplingFactory;
import rectification.transformation.Transformation;
import rectification.transformation.TransformationFactory;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author richardbischof
 */
public class RectifyJob extends JobBase<RectifyJobParameters> {

    private static final String EPSG25832 = "PROJCS[\"ETRS89 / UTM zone 32N\",GEOGCS[\"ETRS89\",DATUM[\"European_Terrestrial_Reference_System_1989\",SPHEROID[\"GRS 1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY[\"EPSG\",\"6258\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4258\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",9],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"25832\"]]";

    public RectifyJob(Image input, RectifyJobParameters parameter) {
        super(input, parameter);
    }

    @Override
    public Image run() {

        Transformation stt = sourceToTarget(parameter);

        WorldData twd = stt.getTargetWorldDataWithBuffer(
                input.getImage().getWidth(),
                input.getImage().getHeight(),
                parameter.getBuffer());

        Transformation tts = targetToSource(parameter, twd);

        result.setImage(new BufferedImage(twd.getWidthInPixel(), twd.getHeightInPixel(), input.getImage().getType()));

        Coefficient coefficient = tts.getCoefficients();
        BufferedImage inputimage = input.getImage();
        BufferedImage output = result.getImage();

        Queue<Worker> workers = new LinkedBlockingQueue<>();

        Resampling resampling = ResamplingFactory.create(parameter.getResamplingType());

        int size = 500;
        for (int y = 0; y < output.getHeight(); y += size) {
            for (int x = 0; x < output.getWidth(); x += size) {
                int maxX = x + size > output.getWidth() ? x + output.getWidth() % size : x + size;
                int maxY = y + size > output.getHeight() ? y + output.getHeight() % size : y + size;
                Worker worker = new Worker(coefficient, resampling, inputimage, output, x, maxX, y, maxY);
                workers.add(worker);
            }
        }
        ExecutorService taskExecutor = Executors.newFixedThreadPool(1);
        while (!workers.isEmpty()) {
            taskExecutor.execute(workers.poll());
        }
        taskExecutor.shutdown();
        try {
            taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {

        }

        CoordinateReferenceSystem crs = null;
        try {
//            crs = CRS.decode("EPSG:25832");
            crs = CRS.parseWKT(EPSG25832);
        } catch (FactoryException e) {
            e.printStackTrace();
        }

        Envelope env = new ReferencedEnvelope(twd.getMinX(),twd.getMaxX(),twd.getMinY(),twd.getMaxY(),crs);
        this.result.setEnvelope(env);
        this.result.setFootprint(twd.getFootprint());
        this.result.setRootMeanSquareError(stt.getRootMeanSquareError());

        return result;

    }

    private Transformation sourceToTarget(RectifyJobParameters parameter) {
        return TransformationFactory.create(parameter.getTransformationMethod(), parameter.getGroundControlPointList());
    }

    private Transformation targetToSource(RectifyJobParameters parameter, WorldData twd) {
        List<GroundControlPoint> gcps = parameter.getGroundControlPointList().stream().map(gcp ->
                convertGroundControlPoint(gcp, twd)).collect(Collectors.toList());
        return TransformationFactory.create(parameter.getTransformationMethod(), gcps);
    }

    private static GroundControlPoint convertGroundControlPoint(GroundControlPoint gcp, WorldData twd) {
        double x = (gcp.dstx - twd.getMinX()) / twd.getPixelWidthInMeter();
        double y = (twd.getMaxY() - gcp.dsty) / twd.getPixelHeightInMeter();
        return new GroundControlPoint(x, y, gcp.srcx,gcp.srcy);
    }
}
