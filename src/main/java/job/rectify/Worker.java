package job.rectify;

import rectification.resampling.Resampling;
import rectification.transformation.Coefficient;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Worker implements Runnable {

    private Resampling resampling;

    private Coefficient k;
    private BufferedImage input;
    private BufferedImage output;
    private int xMin, xMax, yMin, yMax;

    private double xxMin;			// grid origin
    private double yyMin;
    private double dx;				// grid spacing
    private double dy;

    public Worker(Coefficient k, Resampling resampling, BufferedImage input, BufferedImage output, int xMin, int xMax, int yMin, int yMax) {
        this.k = k;
        this.input = input;
        this.output = output;
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;

        this.xxMin = 0;
        this.yyMin = 0;
        this.dx = 1;
        this.dy = 1;
        this.resampling = resampling;
    }

    public BufferedImage rectify() {

        for (int gidY = yMin; gidY < yMax; gidY++) {

            for (int gidX = xMin; gidX < xMax; gidX++) {

                double i = (k.dxxx * gidX * gidX) + (k.dxyy * gidY * gidY) + (k.dxxy * gidX * gidY) + (k.dxx * gidX) + (k.dxy * gidY) + k.dx;
                double j = (k.dyxx * gidX * gidX) + (k.dyyy * gidY * gidY) + (k.dyxy * gidX * gidY) + (k.dyx * gidX) + (k.dyy * gidY) + k.dy;

                if (i >= 0 && i < input.getWidth() - 1 && j >= 0 && j < input.getHeight() - 1) {
                    int val = (int) resampling.getPixelValue(input, i, j, xxMin, yyMin, dx, dy);
                    output.setRGB(gidX, gidY, new Color(val,val,val).getRGB());
                }
            }
        }
        return output;
    }

    @Override
    public void run() {
        this.rectify();
    }

}
