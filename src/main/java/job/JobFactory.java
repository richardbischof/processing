package job;

import job.crop.CropJob;
import job.crop.CropJobParameters;
import job.rectify.RectifyJob;
import job.rectify.RectifyJobParameters;

/**
 * @author richardbischof
 */
public class JobFactory <T extends JobParameter> {

    public Job create(JobType type, Image input, T parameter) {

        Job job;

        switch (type) {
            case CROP:
                job = new CropJob(input, (CropJobParameters) parameter);
                break;
            case RECTIFY:
                job = new RectifyJob(input, (RectifyJobParameters) parameter);
                break;
            default:
                throw new IllegalArgumentException("Parameter not valid.");
        }
        return job;
    }
}
