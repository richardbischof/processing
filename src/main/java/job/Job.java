package job;

import job.JobParameter;

/**
 * @author richardbischof
 */
public interface Job<T extends JobParameter> {

    Image run();

}
