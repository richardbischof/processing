package job;

import job.Job;
import job.JobParameter;


/**
 * @author richardbischof
 */
public abstract class JobBase<T extends JobParameter> implements Job {

    protected Image input;
    protected Image result;
    protected T parameter;

    public JobBase() {
    }

    public JobBase(Image input, T parameter) {
        this.input = input;
        this.result = new Image();
        this.parameter = parameter;
    }
}
