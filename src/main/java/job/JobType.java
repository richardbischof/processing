package job;

/**
 * @author richardbischof
 */
public enum JobType {
    CROP,
    RECTIFY
}
