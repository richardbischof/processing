package job.crop;

import job.Image;
import job.JobBase;

/**
 * @author richardbischof
 */
public class CropJob extends JobBase<CropJobParameters> {

    public CropJob(Image input, CropJobParameters parameter) {
        super(input, parameter);
    }

    @Override
    public Image run() {
        this.result.setImage(
                this.input.getImage().getSubimage(
                        this.parameter.getX(),
                        this.parameter.getY(),
                        this.parameter.getW(),
                        this.parameter.getH()));
        return this.result;
    }
}
