package job.crop;

import job.JobParameter;

/**
 * @author richardbischof
 */
public class CropJobParameters extends JobParameter {

    private int x, y, w, h;

    public CropJobParameters(CropData cropData) {
        this.x = cropData.x;
        this.y = cropData.y;
        this.w = cropData.w;
        this.h = cropData.h;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    @Override
    public String toString() {
        return "job.crop.CropJobParameters{" +
                "x=" + x +
                ", y=" + y +
                ", w=" + w +
                ", h=" + h +
                '}';
    }
}
