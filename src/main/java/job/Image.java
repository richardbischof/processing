package job;

import com.vividsolutions.jts.geom.Polygon;
import org.opengis.geometry.Envelope;

import javax.imageio.metadata.IIOMetadata;
import java.awt.image.BufferedImage;

/**
 * @author richardbischof
 */
public class Image {

    private IIOMetadata metadata;
    private BufferedImage image;
    private Envelope envelope;
    private Polygon footprint;
    private double rootMeanSquareError;

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }

    public Image() {
    }

    public Image(IIOMetadata metadata, BufferedImage image) {
        this.metadata = metadata;
        this.image = image;
    }

    public IIOMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(IIOMetadata metadata) {
        this.metadata = metadata;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public Polygon getFootprint() {
        return footprint;
    }

    public void setFootprint(Polygon footprint) {
        this.footprint = footprint;
    }

    @Override
    public String toString() {
        return "job.Image{" +
                "metadata=" + metadata +
                ", image=" + image +
                '}';
    }

    public void setRootMeanSquareError(double rootMeanSquareError) {
        this.rootMeanSquareError = rootMeanSquareError;
    }

    public double getRootMeanSquareError() {
        return rootMeanSquareError;
    }
}
